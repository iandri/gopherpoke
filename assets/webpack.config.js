var path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'js'),
    filename: 'gopherpoke.min.js'
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      components: path.resolve(__dirname, 'src/components'),
      "react": 'preact-compat',
      "react-dom": 'preact-compat'
    }
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: 'buble-loader',
        include: path.join(__dirname, 'src'),
        query: {
          jsx: 'h',
          objectAssign: 'Object.assign'
        }
      }
    ]
  }
};

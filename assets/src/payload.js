class Payload {
  constructor (service, host, repo, nick, email) {
    this.service = service
    this.host = host
    this.repo = repo
    this.nick = nick
    this.email = email
  }

  point (issue, value) {
    return {
      service: this.service,
      host: this.host,
      repo: this.repo,
      issue: issue,
      score: value
    }
  }

  story (issue, title, description) {
    return {
      service: this.service,
      host: this.host,
      repo: this.repo,
      issue: issue,
      title: title,
      description: description
    }
  }

  register (service, host, repo, nick, email) {
    this.service = service
    this.host = host
    this.repo = repo
    this.nick = nick
    this.email = email

    return {
      service: service,
      host: host,
      repo: repo,
      nick: nick,
      email: email
    }
  }
}

const payload = new Payload()
export default payload

export const actions = {
  Register: 1,
  CreateStory: 2,
  RemoveStory: 3,
  PointStory: 4
}

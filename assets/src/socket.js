class Socket {
  constructor (uri) {
    this.conn = null
  }

  test () {
    return window['WebSocket']
  }

  init (uri) {
    if (!this.test()) {
      this.warn('WebSocket is not available')
      return null
    }

    if (this.conn) {
      this.log('using previous socket')
      return this.conn
    }

    this.log('creating new WebSocket')
    this.conn = new window.WebSocket('ws://' + uri + '/socket')
    return this.conn
  }

  send (action, data) {
    if (!action || !data || !this.conn) {
      this.warn('failed to send payload')
      return false
    }

    this.log('sending payload')
    this.conn.send(JSON.stringify({action: action, data: data }))
    return true
  }

  listen (onClose, onMessage) {
    if (!onClose || !onMessage || !this.conn) {
      this.warn('failed to listen')
      return false
    }

    this.log('listening to socket')
    this.conn.onclose = onClose
    this.conn.onmessage = onMessage
    return true
  }

  log (message) {
    console.log('SOCKET: ' + message)
  }

  warn (message) {
    console.warn('SOCKET: ' + message)
  }
}

const socket = new Socket()
export default socket

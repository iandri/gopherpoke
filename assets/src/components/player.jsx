import { h, render, Component } from 'preact'
import md5 from 'crypto-js/md5'

export default class Player extends Component {
  render ({ email = '', nick = 'Unknown' }) {
    // Gravatar for icons
    // Reference: https://secure.gravatar.com/site/implement/images/
    const hash = md5(email)
    const gravatar = `https://www.gravatar.com/avatar/${hash}`

    return (
      <span className="card player">
        <img className="player-icon" src={gravatar} alt="" />
        <span className="player-nick">{nick}</span>
      </span>
    )
  }
}

import { h, render, Component } from 'preact'
import Card from './card'

export default class Deck extends Component {
  render ({ cards = ['0', '1', '2', '3', '5', '8', '13', '21', ':coffee:'] }) {
    const children = cards.map(v => {
      return <Card value={v} />
    })

    return (<span className="deck">{children}</span>)
  }
}

import { h, render } from 'preact';
import App from './components/app'

let root = null
function init() {
  root = render(<App />, document.body, root);
}
init()

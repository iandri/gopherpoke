package main

import (
	"log"
	"net/http"

	"gitlab.com/revolvingcow/gopherpoke/sockets"
)

func main() {
	log.SetFlags(log.Lshortfile)

	// Websocket server
	server := sockets.NewServer("/socket")
	go server.Listen()

	// Static files
	http.Handle("/", http.FileServer(http.Dir("assets")))
	log.Fatal(http.ListenAndServe(":8081", nil))
}

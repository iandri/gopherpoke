package sockets

import (
	"log"
	"net/http"

	"gitlab.com/revolvingcow/gopherpoke/models"
	"golang.org/x/net/websocket"
)

type Server struct {
	pattern   string
	queue     []*models.Payload
	clients   map[int]*Client
	add       chan *Client
	del       chan *Client
	broadcast chan *models.Payload
	done      chan bool
	errors    chan error
}

func NewServer(pattern string) *Server {
	return &Server{
		pattern,
		[]*models.Payload{},
		make(map[int]*Client),
		make(chan *Client),
		make(chan *Client),
		make(chan *models.Payload),
		make(chan bool),
		make(chan error),
	}
}

func (s *Server) Add(c *Client) {
	s.add <- c
}

func (s *Server) Del(c *Client) {
	s.del <- c
}

func (s *Server) SendAll(payload *models.Payload) {
	s.broadcast <- payload
}

func (s *Server) Done() {
	s.done <- true
}

func (s *Server) Err(err error) {
	s.errors <- err
}

func (s *Server) sendPastMessages(c *Client) {
	for _, payload := range s.queue {
		c.Write(payload)
	}
}

func (s *Server) sendAll(payload *models.Payload) {
	for _, c := range s.clients {
		c.Write(payload)
	}
}

func (s *Server) Listen() {
	log.Println("Listening server...")

	// websocket handler
	onConnected := func(ws *websocket.Conn) {
		defer func() {
			if err := ws.Close(); err != nil {
				s.errors <- err
			}
		}()

		client := NewClient(ws, s)
		s.Add(client)
		client.Listen()
	}
	http.Handle(s.pattern, websocket.Handler(onConnected))
	log.Println("Created handler")

	for {
		select {

		// Add new a client
		case c := <-s.add:
			log.Println("Added new client")
			s.clients[c.id] = c
			log.Println("Now", len(s.clients), "clients connected.")
			s.sendPastMessages(c)

		// del a client
		case c := <-s.del:
			log.Println("Delete client")
			delete(s.clients, c.id)

		// broadcast message for all clients
		case payload := <-s.broadcast:
			log.Println("Send all:", payload)
			s.queue = append(s.queue, payload)
			s.sendAll(payload)

		case err := <-s.errors:
			log.Println("Error:", err.Error())

		case <-s.done:
			return
		}
	}
}

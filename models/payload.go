package models

const (
	// Register the repository for a poker session. If one
	// is already created then treat this as if you were
	// joining the room.
	Register = iota

	// CreateStory will update the story if it exists.
	// If it does not then it will be created.
	CreateStory

	// RemoveStory will remove the story from the backlog.
	RemoveStory

	// PointStory will create or update your vote on
	// a particular story in the backlog.
	PointStory
)

// Payload is the initial message sent across the websocket.
//
// Action can be any of the following:
//  - 1 => register the room
//  - 2 => create a story
//  - 3 => remove a story
//  - 4 => point a story
//
// Data is an anonymous interface which should be deserialized
// based on the action.
type Payload struct {
	Action int         `json:"action"`
	Data   interface{} `json:"data"`
}

// Registration is the payload data sent to create a persistent
// store of the poker game as well as identifying the user.
type Registration struct {
	Service    string `json:"service"`
	Host       string `json:"host"`
	Repository string `json:"repo"`
	Nick       string `json:"nick"`
	Email      string `json:"email,omitempty"`
}

// Story is the payload data sent to create or remove a story from
// the backlog during the pointing process.
type Story struct {
	Service     string `json:"service"`
	Host        string `json:"host"`
	Repository  string `json:"repo"`
	Issue       int    `json:"issue"`
	Title       string `json:"title,omitempty"`
	Description string `json:"descripton,omitempty"`
	Score       string `json:"score,omitempty"`
}
